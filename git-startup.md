## SSH setup

Add an ssh key to your environment session: 
```bash
eval $(ssh-agent)
ssh-add ~/.ssh/<path_to_your_ssh_key>
```

Set up your git user and email. You only need to run this once:
```bash
git config user.name "Your Name Here"
git config user.email your@email.example
```

Install all required software 
```bash
sed -i "s/#\$nrconf{restart} = 'i';/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf; sed -ie 's/PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config; sed -ie 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config; apt update && apt upgrade -y && apt install minetest minetest-server -y
````

Create server user
```bash
useradd server; mkdir -p /home/server/minetest; echo "SHELL=/bin/bash" >> /home/server/.profile; chown -R server:server /home/server; su -l server; PATH=$PATH:/usr/games
```

Configure minetest
```bash
echo -e "server_announce = false\nserver_name = \"test server - will delete\"\nserver_description = \"This is just a test server\"\nmotd = \"Welcome\"" > ~/minetest/minetest.conf
```

#### TO DO: 
- Disable PermitRootLogin yes
- run command: minetestserver --logfile /home/server/minetest/minetest.log
- mkdir -p /home/server/minetest/.minetest/worlds/
- run: minetestserver --world ~/minetest/.minetest/worlds/my_world --logfile ~/minetest/minetest.log


